package implementation;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.security.auth.x500.X500Principal;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.PKCS10CertificationRequestBuilder;
import org.bouncycastle.pkcs.PKCSException;
import org.bouncycastle.pkcs.jcajce.JcaPKCS10CertificationRequestBuilder;

import code.GuiException;
import x509.v3.CodeV3;

public class MyCode extends CodeV3 {
	private static final String LOCALPASS = "fakinLocalPass";
	private static final int CERTIFICATE_VERSION_V3 = 2;
	private static final int CPS_URI_CRITICAL = 3;
	private static final int ALT_NAME_CRITICAL = 6;
	private static final int BASIC_CONST_CRITICAL = 8;

	private KeyStore localKeyStore;
	private CertificateBuilderWrapper currentCertificateBuilderWrapper;
	private ArrayList<CertificateBuilderWrapper> unsignedCertificates;
	private X509Certificate selectedCertificate;
	private X509Certificate issuerCertificate;
	private HashMap<String, String> passwordHolder = new HashMap<>();

	// 3 CPS-URI, 6 ISSUER ALT NAME, 8 BASIC CONSTRAINTS access.isCritical(i)
	public MyCode(boolean[] algorithm_conf, boolean[] extensions_conf) throws GuiException {
		super(algorithm_conf, extensions_conf);

		unsignedCertificates = new ArrayList<>();

		Security.addProvider(new BouncyCastleProvider());

		access.setVersion(2);

	}

	@Override
	public boolean exportCertificate(File file, int encoding) {
		// encoding: 0 - DER, 1 - PEM
		if (selectedCertificate != null) {
			if (encoding == 1) {
				try {
					String encoded = encodeToPem(selectedCertificate);
					FileOutputStream fos = new FileOutputStream(file);
					fos.write(encoded.getBytes());
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				// DER
				FileOutputStream fos = null;
				try {
					fos = new FileOutputStream(file);
					fos.write(encodeToDer(selectedCertificate));
					fos.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (CertificateEncodingException e) {
					e.printStackTrace();
				} finally {
					try {
						fos.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

			}
			return true;
		}
		return false;
	}

	@SuppressWarnings("static-access")
	@Override
	public boolean exportKeypair(String keypairName, String filePath, String password) {
		FileOutputStream fos = null;
		try {
			if (localKeyStore.containsAlias(keypairName)) {
				KeyStore keyStore = keyStoreInit();
				keyStore.setKeyEntry(keypairName, localKeyStore.getKey(keypairName, LOCALPASS.toCharArray()),
						password.toCharArray(), localKeyStore.getCertificateChain(keypairName));
				fos = new FileOutputStream(filePath + ".p12");
				keyStore.store(fos, password.toCharArray());
				return true;
			} else {
				access.reportError("Certificate is not signed!");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (KeyStoreException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (UnrecoverableKeyException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fos != null) {
					fos.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return false;
	}

	@Override
	public boolean generateCSR(String arg0) {
		try {
			ContentSigner signGen = new JcaContentSignerBuilder(currentCertificateBuilderWrapper.getAlgorithm())
					.build(currentCertificateBuilderWrapper.getKeyPair().getPrivate());
			PKCS10CertificationRequestBuilder bil = new JcaPKCS10CertificationRequestBuilder(
					currentCertificateBuilderWrapper.getSubject(),
					currentCertificateBuilderWrapper.getKeyPair().getPublic());
			PKCS10CertificationRequest csr = bil.build(signGen);
			currentCertificateBuilderWrapper.setCsr(csr);
			// debug
			StringWriter sw = new StringWriter();
			JcaPEMWriter pem = new JcaPEMWriter(sw);
			pem.writeObject(csr);
			pem.close();

			System.out.println(sw.toString());
		} catch (OperatorCreationException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public String getIssuer(String keypairName) {
		try {
			Certificate[] cc = localKeyStore.getCertificateChain(keypairName);
			return ((X509Certificate) cc[0]).getIssuerX500Principal().getName();
		} catch (KeyStoreException e) {
			e.printStackTrace();
		}
		return "C=SER,O=ETF,OU=SI,CN=ZP,L=BG"; // odvaja se zarezom | ne treba vise | primer return-a
	}

	@Override
	public String getIssuerPublicKeyAlgorithm(String keypairName) {
		try {
			Certificate[] cc = localKeyStore.getCertificateChain(keypairName);
			issuerCertificate = (X509Certificate) cc[0];
			return ((X509Certificate) cc[0]).getPublicKey().getAlgorithm();
		} catch (KeyStoreException e) {
			e.printStackTrace();
		}
		return "blin";
	}

	@Override
	public List<String> getIssuers(String keypairName) {
		List<String> issuers = new ArrayList<>();
		try {
			Enumeration<String> alliases = localKeyStore.aliases();
			while (alliases.hasMoreElements()) {
				String alias = alliases.nextElement();
				if (localKeyStore.isKeyEntry(alias)) {
					Certificate[] cc = localKeyStore.getCertificateChain(alias);
					if (((X509Certificate) cc[0]).getBasicConstraints() <= currentCertificateBuilderWrapper
							.getCaPathLength()) {
						continue;
					}
					int br = 0;
					for (int i = 0; i < cc.length; i++) {
						if (((X509Certificate) cc[i]).getBasicConstraints() >= i) {
							if (currentCertificateBuilderWrapper.getCaPathLength() > -1
									&& ((X509Certificate) cc[0]).getBasicConstraints() == 0) {
								break;
							}
							br++;
						}
					}
					if (br == cc.length) {
						issuers.add(alias);
					}
				}
			}
		} catch (KeyStoreException e) {
			e.printStackTrace();
		}
		return issuers;
	}

	@Override
	public int getRSAKeyLength(String keypairName) {
		if (issuerCertificate.getPublicKey().getAlgorithm().equals("RSA")) {
			return ((RSAPublicKey) issuerCertificate.getPublicKey()).getModulus().bitLength();
		}
		return 0;
	}

	@Override
	public boolean importCertificate(File file, String keypairName) {
		CertificateFactory cf;
		try {
			cf = CertificateFactory.getInstance("X.509");
			FileInputStream fis = new FileInputStream(file);
			X509Certificate c = (X509Certificate) cf.generateCertificate(fis);
			localKeyStore.setCertificateEntry(keypairName, c);
			return true;
		} catch (CertificateException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (KeyStoreException e) {
			e.printStackTrace();
		}
		return false;
	}

	private void writeCertificateGUI(X509Certificate c, X509Certificate issuerCertificate) throws IOException {
		access.setVersion(c.getVersion() - 1);
		access.setSerialNumber(c.getSerialNumber() + "");
		access.setNotBefore(c.getNotBefore());
		access.setNotAfter(c.getNotAfter());
		// CN=eqe,OU=eqw,O=eq,L=q,ST=e,C=qw
		X500Principal p = c.getSubjectX500Principal();
		String[] parts = p.getName().split(",");
		String country = "";
		String state = "";
		String locality = "";
		String organization = "";
		String organizationUnit = "";
		String commonName = "";
		for (int i = 0; i < parts.length; i++) {
			String[] secondPart = parts[i].split("=");
			switch (secondPart[0]) {
			case "C":
				if (secondPart.length > 1) {
					country = parts[i].split("=")[1];
				}
				break;
			case "ST":
				if (secondPart.length > 1) {
					state = parts[i].split("=")[1];
				}
				break;
			case "L":
				if (secondPart.length > 1) {
					locality = parts[i].split("=")[1];
				}
				break;
			case "O":
				if (secondPart.length > 1) {
					organization = parts[i].split("=")[1];
				}
				break;
			case "OU":
				if (secondPart.length > 1) {
					organizationUnit = parts[i].split("=")[1];
				}
				break;
			case "CN":
				if (secondPart.length > 1) {
					commonName = parts[i].split("=")[1];
				}
				break;
			}
		}
		access.setSubjectCountry(country);
		access.setSubjectState(state);
		access.setSubjectLocality(locality);
		access.setSubjectOrganization(organization);
		access.setSubjectOrganizationUnit(organizationUnit);
		access.setSubjectCommonName(commonName);
		access.setSubjectSignatureAlgorithm(c.getSigAlgName());
		access.setPublicKeySignatureAlgorithm(c.getSigAlgName());
		access.setPublicKeyAlgorithm("RSA");
		access.setPublicKeyParameter(((RSAPublicKey) c.getPublicKey()).getModulus().bitLength() + "");
		access.setIssuer(c.getIssuerX500Principal().getName());
		if (issuerCertificate != null) {
			access.setIssuerSignatureAlgorithm(issuerCertificate.getSigAlgName());
		} else {
			access.setIssuerSignatureAlgorithm(c.getSigAlgName());
		}
		/*
		String partyName = "";
		byte[] extensionValue = c.getExtensionValue(Extension.issuerAlternativeName.getId());
		if (extensionValue != null) {
			ASN1Primitive derObj = JcaX509ExtensionUtils.parseExtensionValue(extensionValue);
			System.out.println(derObj.toString());
			String[] ediPart = derObj.toString().split("\\[5\\]");
			if (ediPart.length > 1) {
				partyName = ediPart[1].split("(,|\\])")[0];
			}
		}
		*/
		try {
			String issuerAltNamesString = "";
			Collection<List<?>> issuerAltNamesCol = c.getIssuerAlternativeNames();
			if (issuerAltNamesCol != null) {
				for (List<?> issuersAltNames : issuerAltNamesCol) {
					for (Object o : issuersAltNames) {
						if (o instanceof String) {
							String s = (String) o;
							issuerAltNamesString += s + ",";
						}
					}
				}
				// edi deo
				/*if (partyName.length() > 0) {
					issuerAltNamesString += "edi:" + partyName + ",";
				}*/
				issuerAltNamesString = issuerAltNamesString.substring(0, issuerAltNamesString.length() - 1);
				access.setAlternativeName(ALT_NAME_CRITICAL, issuerAltNamesString);
			}
		} catch (CertificateParsingException e) {
			e.printStackTrace();
		}
		
		if (c.getExtensionValue(Extension.certificatePolicies.getId()) != null) {
			String cpsUriParts = new String(c.getExtensionValue(Extension.certificatePolicies.getId()));
			String cpsUri = cpsUriParts.split("http")[1];
			cpsUri = "http" + cpsUri;
			access.setCpsUri(cpsUri);
			access.setAnyPolicy(true);
		}
		
		int basicConst = c.getBasicConstraints();
		if (basicConst > -1) {
			access.setCA(true);
			access.setPathLen(basicConst + "");
		}
		
		Set<String> criticalExtensions = c.getCriticalExtensionOIDs();
		for (String extension : criticalExtensions) {
			if (Extension.basicConstraints.getId().equals(extension)) {
				access.setCritical(BASIC_CONST_CRITICAL, true);
			}
			if (Extension.issuerAlternativeName.getId().equals(extension)) {
				access.setCritical(ALT_NAME_CRITICAL, true);
			}
			if (Extension.certificatePolicies.getId().equals(extension)) {
				access.setCritical(CPS_URI_CRITICAL, true);
			}
		}
	}

	@SuppressWarnings("static-access")
	@Override
	public boolean importKeypair(String keypairName, String filePath, String password) {
		try {
			KeyStore keyStore = KeyStore.getInstance("pkcs12");
			FileInputStream fis = new FileInputStream(filePath);
			keyStore.load(fis, password.toCharArray());
			Enumeration<String> a = keyStore.aliases();
			while (a.hasMoreElements()) {
				String alias = a.nextElement();
				passwordHolder.put(alias, password);
				if (keyStore.isKeyEntry(alias)) {
					localKeyStore.setKeyEntry(alias, keyStore.getKey(alias, password.toCharArray()),
							password.toCharArray(), keyStore.getCertificateChain(alias));
				}
			}
			return true;
		} catch (KeyStoreException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			access.reportError("Bad file, or password");

		} catch (UnrecoverableKeyException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public int loadKeypair(String keypairName) {
		access.enableExportButton(true);
		try {
			if (localKeyStore.isKeyEntry(keypairName)) {
				Certificate[] certChain = localKeyStore.getCertificateChain(keypairName);
				selectedCertificate = (X509Certificate) certChain[0];
				if (certChain.length > 1) {
					writeCertificateGUI(selectedCertificate, (X509Certificate) certChain[1]);
				} else {
					writeCertificateGUI(selectedCertificate, null);
				}
				return 1;
			}
			if (localKeyStore.isCertificateEntry(keypairName)) {
				X509Certificate cert = (X509Certificate) localKeyStore.getCertificate(keypairName);
				writeCertificateGUI(cert, null);
				return 2;
			}
			selectedCertificate = null;
			for (CertificateBuilderWrapper cbw : unsignedCertificates) {
				if (cbw.getKeypairName().equals(keypairName)) {
					access.setVersion(2);
					access.setSerialNumber(cbw.getSerialNumber().toString());
					access.setNotBefore(cbw.getNotBefore());
					access.setNotAfter(cbw.getNotAfter());
					access.setSubjectCountry(cbw.getSubjectCountry());
					access.setSubjectState(cbw.getSubjectState());
					access.setSubjectLocality(cbw.getSubjectLocality());
					access.setSubjectOrganization(cbw.getSubjectOrganization());
					access.setSubjectOrganizationUnit(cbw.getSubjectOrganizationUnit());
					access.setSubjectCommonName(cbw.getSubjectCommonName());
					access.setSubjectSignatureAlgorithm(cbw.getAlgorithm());
					access.setPublicKeySignatureAlgorithm(cbw.getAlgorithm());
					access.setPublicKeyAlgorithm("RSA");
					access.setPublicKeyParameter(cbw.getRsaKeyLength() + "");
					if (cbw.isCriticalCerPolicies()) {
						access.setCritical(CPS_URI_CRITICAL, true);
					}
					if (cbw.isCriticalAltNames()) {
						access.setCritical(ALT_NAME_CRITICAL, true);
					}
					if (cbw.isCriticalBasicConst()) {
						access.setCritical(BASIC_CONST_CRITICAL, true);
					}
					if (cbw.isAnyPolicyChecked()) {
						access.setAnyPolicy(true);
						access.setCpsUri(cbw.getCpsUri());
					}
					if (cbw.getCaPathLength() > -1) {
						access.setCA(true);
						access.setPathLen(cbw.getCaPathLength() + "");
					}
					if (cbw.getIssuerAltNames() != null) {
						String names = "";
						for (String name : cbw.getIssuerAltNames()) {
							names += name;
						}
						access.setAlternativeName(ALT_NAME_CRITICAL, names);
					}
					currentCertificateBuilderWrapper = cbw;
					return 0;
				}
			}
		} catch (KeyStoreException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return 0;
	}

	@Override
	public Enumeration<String> loadLocalKeystore() {
		try {
			if (localKeyStore == null) {
				localKeyStore = keyStoreInit();
			}
			return localKeyStore.aliases();
		} catch (KeyStoreException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean removeKeypair(String keypairName) {
		try {
			if (localKeyStore.containsAlias(keypairName)) {
				localKeyStore.deleteEntry(keypairName);
				return true;
			}
			for (CertificateBuilderWrapper cb : unsignedCertificates) {
				if (cb.getKeypairName().equals(keypairName)) {
					unsignedCertificates.remove(cb); // unsafe testiraj.
					return true;
				}
			}
		} catch (KeyStoreException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public void resetLocalKeystore() {
		try {
			localKeyStore = keyStoreInit();
			unsignedCertificates.clear();
		} catch (KeyStoreException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("static-access")
	@Override
	public boolean saveKeypair(String keypairName) {
		if (access.getVersion() == CERTIFICATE_VERSION_V3) {
			currentCertificateBuilderWrapper = new CertificateBuilderWrapper();
			currentCertificateBuilderWrapper.setAlgorithm(access.getPublicKeySignatureAlgorithm());
			currentCertificateBuilderWrapper.setSerialNumber(new BigInteger(access.getSerialNumber()));
			currentCertificateBuilderWrapper.setNotBefore(access.getNotBefore());
			currentCertificateBuilderWrapper.setNotAfter(access.getNotAfter());
			try {
				KeyPair kp = createRSAKeyPair(Integer.parseInt(access.getPublicKeyParameter()));
				currentCertificateBuilderWrapper.setKeyPair(kp);

			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}

			String subjectCountry = access.getSubjectCountry();
			String subjectState = access.getSubjectState();
			String subjectLocality = access.getSubjectLocality();
			String subjectOrganization = access.getSubjectOrganization();
			String subjectOrganizationUnit = access.getSubjectOrganizationUnit();
			String subjectCommonName = access.getSubjectCommonName();
			currentCertificateBuilderWrapper.setSubjectCountry(subjectCountry);
			currentCertificateBuilderWrapper.setSubjectState(subjectState);
			currentCertificateBuilderWrapper.setSubjectLocality(subjectLocality);
			currentCertificateBuilderWrapper.setSubjectOrganization(subjectOrganization);
			currentCertificateBuilderWrapper.setSubjectOrganizationUnit(subjectOrganizationUnit);
			currentCertificateBuilderWrapper.setSubjectCommonName(subjectCommonName);

			X500Name subject = new X500Name("C=" + subjectCountry + ", ST=" + subjectState + ", L=" + subjectLocality
					+ ", O=" + subjectOrganization + ", OU=" + subjectOrganizationUnit + ", CN=" + subjectCommonName);
			currentCertificateBuilderWrapper.setSubject(subject);
			currentCertificateBuilderWrapper.setAlgorithm(access.getPublicKeySignatureAlgorithm());
			currentCertificateBuilderWrapper.setRsaKeyLength(Integer.parseInt(access.getPublicKeyParameter()));
			currentCertificateBuilderWrapper.setKeypairName(keypairName);
			if (access.isCA()) {
				currentCertificateBuilderWrapper.setCaPathLength(Integer.parseInt(access.getPathLen()));
			} else {
				currentCertificateBuilderWrapper.setCaPathLength(-1);
			}
			currentCertificateBuilderWrapper.setCriticalCerPolicies(access.isCritical(CPS_URI_CRITICAL));
			currentCertificateBuilderWrapper.setCriticalAltNames(access.isCritical(ALT_NAME_CRITICAL));
			currentCertificateBuilderWrapper.setCriticalBasicConst(access.isCritical(BASIC_CONST_CRITICAL));
			currentCertificateBuilderWrapper.setCpsUri(access.getCpsUri());
			currentCertificateBuilderWrapper.setIssuerAltNames(access.getAlternativeName(ALT_NAME_CRITICAL));
			if (!access.getCpsUri().equals("")) {
				currentCertificateBuilderWrapper.setAnyPolicyChecked(access.getAnyPolicy());
			}
			passwordHolder.put(keypairName, LOCALPASS);

			unsignedCertificates.add(currentCertificateBuilderWrapper);
			return true;
		} else {
			access.reportError("VERSION 3 ONLY");
			return false;
		}
	}

	@SuppressWarnings("static-access")
	@Override
	public boolean signCertificate(String issuer, String algorithm) {
		issuerCertificate = null;
		String i = getIssuer(issuer);
		X500Name x500Issuer = new X500Name(i);
		currentCertificateBuilderWrapper.setIssuer(x500Issuer);
		if (!algorithm.equals(currentCertificateBuilderWrapper.getAlgorithm())) {
			access.reportError("Algorithms doesn't match");
			return false;
		}
		PrivateKey pk;
		try {
			pk = (PrivateKey) localKeyStore.getKey(issuer, passwordHolder.get(issuer).toCharArray());
			ContentSigner contentSigner = new JcaContentSignerBuilder(algorithm).build(pk);
			X509Certificate issuerCert = (X509Certificate) localKeyStore.getCertificateChain(issuer)[0];
			X509CertificateHolder certificateHolder = currentCertificateBuilderWrapper.buildWithIssuerCert(issuerCert,
					contentSigner);
			InputStream is = new ByteArrayInputStream(certificateHolder.toASN1Structure().getEncoded());
			CertificateFactory cf = CertificateFactory.getInstance("X509", "BC");
			X509Certificate certificate = (X509Certificate) cf.generateCertificate(is);
			List<Certificate> chainList = new ArrayList<>();
			Certificate[] issuerCC = localKeyStore.getCertificateChain(issuer);
			chainList.add(certificate);
			for (Certificate c : issuerCC) {
				chainList.add(c);
			}
			issuerCC = chainList.toArray(new Certificate[1 + issuerCC.length]);
			Certificate[] certChain = issuerCC;
			String keypairName = currentCertificateBuilderWrapper.getKeypairName();
			localKeyStore.setKeyEntry(keypairName, currentCertificateBuilderWrapper.getKeyPair().getPrivate(),
					LOCALPASS.toCharArray(), certChain);
			unsignedCertificates.remove(currentCertificateBuilderWrapper);

			certificate.verify(issuerCert.getPublicKey());
			System.out.println("SVE JE OK"); // debug

		} catch (UnrecoverableKeyException | KeyStoreException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (OperatorCreationException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (SignatureException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		} catch (PKCSException e) {
			e.printStackTrace();
		}

		return true;
	}

	private KeyPair createRSAKeyPair(int bitCount) throws NoSuchAlgorithmException {
		KeyPairGenerator gen = KeyPairGenerator.getInstance("RSA");
		gen.initialize(bitCount);
		return gen.generateKeyPair();
	}

	private String encodeToPem(X509Certificate certificate) throws IOException {
		StringWriter sw = new StringWriter();
		JcaPEMWriter pemWriter = new JcaPEMWriter(sw);
		pemWriter.writeObject(certificate);
		pemWriter.close();
		return sw.toString();
	}

	private byte[] encodeToDer(X509Certificate certificate) throws IOException, CertificateEncodingException {
		return certificate.getEncoded();
	}

	private KeyStore keyStoreInit()
			throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {
		KeyStore ks = KeyStore.getInstance("pkcs12");
		ks.load(null, null);
		return ks;
	}
}

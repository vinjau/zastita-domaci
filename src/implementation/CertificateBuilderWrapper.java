package implementation;

import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.CertificatePolicies;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.PolicyInformation;
import org.bouncycastle.asn1.x509.PolicyQualifierInfo;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.util.PublicKeyFactory;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.ContentVerifierProvider;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentVerifierProviderBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.PKCSException;
import org.bouncycastle.util.IPAddress;

public class CertificateBuilderWrapper {

	private X509v3CertificateBuilder certificateBuilder;
	private PKCS10CertificationRequest csr;
	private BigInteger serialNumber;
	private String algorithm;
	private X500Name subject;
	private X500Name issuer;
	private Date notBefore;
	private Date notAfter;
	private KeyPair keyPair;
	private String keypairName;
	private String subjectCountry;
	private String subjectState;
	private String subjectLocality;
	private String subjectOrganization;
	private String subjectOrganizationUnit;
	private String subjectCommonName;
	private int rsaKeyLength;
	private int caPathLength;
	private String cpsUri;
	private String[] issuerAltNames;
	private boolean criticalCerPolicies;
	private boolean criticalAltNames;
	private boolean criticalBasicConst;
	private boolean anyPolicyChecked;

	public X509v3CertificateBuilder getCertificateBuilder() {
		return certificateBuilder;
	}

	public void setCertificateBuilder(X509v3CertificateBuilder certificateBuilder) {
		this.certificateBuilder = certificateBuilder;
	}

	public String getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}

	public X500Name getSubject() {
		return subject;
	}

	public void setSubject(X500Name subject) {
		this.subject = subject;
	}

	public X500Name getIssuer() {
		return issuer;
	}

	public void setIssuer(X500Name issuer) {
		this.issuer = issuer;
	}

	public Date getNotBefore() {
		return notBefore;
	}

	public void setNotBefore(Date notBefore) {
		this.notBefore = notBefore;
	}

	public Date getNotAfter() {
		return notAfter;
	}

	public void setNotAfter(Date notAfter) {
		this.notAfter = notAfter;
	}

	public KeyPair getKeyPair() {
		return keyPair;
	}

	public void setKeyPair(KeyPair keyPair) {
		this.keyPair = keyPair;
	}

	public String getKeypairName() {
		return keypairName;
	}

	public void setKeypairName(String keypairName) {
		this.keypairName = keypairName;
	}

	public String getSubjectCountry() {
		return subjectCountry;
	}

	public void setSubjectCountry(String subjectCountry) {
		this.subjectCountry = subjectCountry;
	}

	public String getSubjectState() {
		return subjectState;
	}

	public void setSubjectState(String subjectState) {
		this.subjectState = subjectState;
	}

	public String getSubjectLocality() {
		return subjectLocality;
	}

	public void setSubjectLocality(String subjectLocality) {
		this.subjectLocality = subjectLocality;
	}

	public String getSubjectOrganization() {
		return subjectOrganization;
	}

	public void setSubjectOrganization(String subjectOrganization) {
		this.subjectOrganization = subjectOrganization;
	}

	public String getSubjectOrganizationUnit() {
		return subjectOrganizationUnit;
	}

	public void setSubjectOrganizationUnit(String subjectOrganizationUnit) {
		this.subjectOrganizationUnit = subjectOrganizationUnit;
	}

	public String getSubjectCommonName() {
		return subjectCommonName;
	}

	public void setSubjectCommonName(String subjectCommonName) {
		this.subjectCommonName = subjectCommonName;
	}

	public BigInteger getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(BigInteger serialNumber) {
		this.serialNumber = serialNumber;
	}

	public int getRsaKeyLength() {
		return rsaKeyLength;
	}

	public void setRsaKeyLength(int rsaKeyLength) {
		this.rsaKeyLength = rsaKeyLength;
	}

	public X509CertificateHolder buildWithIssuerCert(X509Certificate issuerCertificate, ContentSigner cs)
			throws OperatorCreationException, IOException, NoSuchAlgorithmException, InvalidKeySpecException,
			PKCSException {
		PublicKey pu = getPublicKeyFromCSR();
		JcaContentVerifierProviderBuilder ver = new JcaContentVerifierProviderBuilder();
		ContentVerifierProvider verify = ver.build(pu);
		if (csr.isSignatureValid(verify)) {
			System.out.println("Validan je csr");
		} else {
			System.out.println("NIJE Validan je csr");
		}
		certificateBuilder = new JcaX509v3CertificateBuilder(issuerCertificate, serialNumber, notBefore, notAfter,
				csr.getSubject(), pu);
		if (caPathLength < 0) {
			certificateBuilder.addExtension(Extension.basicConstraints, criticalBasicConst,
					new BasicConstraints(false));
		} else {
			certificateBuilder.addExtension(Extension.basicConstraints, criticalBasicConst,
					new BasicConstraints(caPathLength));
		}

		// Issuer alt names
		if (issuerAltNames.length > 0) {
			GeneralName[] generalNames = new GeneralName[issuerAltNames.length];
			for (int i = 0; i < issuerAltNames.length; i++) {
				boolean isIP = false;
				ASN1ObjectIdentifier asn1obj = null;
				// registeredID
				try {
					asn1obj = ASN1ObjectIdentifier.getInstance(new ASN1ObjectIdentifier(issuerAltNames[i]));
					generalNames[i] = new GeneralName(GeneralName.registeredID, asn1obj);
					continue;
				} catch (Exception e) {
					isIP = true;
				}
				// IP
				/*
				 * Pattern ipPattern =
				 * Pattern.compile("^\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}$"
				 * ); Matcher ipMatcher = ipPattern.matcher(issuerAltNames[i]);
				 */
				if ((IPAddress.isValid(issuerAltNames[i]) || IPAddress.isValidWithNetMask(issuerAltNames[i])) && isIP) {
					generalNames[i] = new GeneralName(GeneralName.iPAddress, issuerAltNames[i]);
					continue;
				}
				// E-mail
				Pattern emailPattern = Pattern.compile("^.+@.+\\..+$");
				Matcher emailMatcher = emailPattern.matcher(issuerAltNames[i]);
				if (emailMatcher.matches()) {
					generalNames[i] = new GeneralName(GeneralName.rfc822Name, new DERIA5String(issuerAltNames[i]));
					continue;
				}
				// URI
				Pattern uriPattern = Pattern.compile("^(http:|ftp:)\\/\\/.+\\..+$");
				Matcher uriMatcher = uriPattern.matcher(issuerAltNames[i].toLowerCase());
				if (uriMatcher.matches()) {
					generalNames[i] = new GeneralName(GeneralName.uniformResourceIdentifier,
							new DERIA5String(issuerAltNames[i]));
					continue;
				}
				// DNS
				Pattern dnsPattern = Pattern.compile("^.+\\..+$");
				Matcher dnsMatcher = dnsPattern.matcher(issuerAltNames[i]);
				if (dnsMatcher.matches()) {
					generalNames[i] = new GeneralName(GeneralName.dNSName, new DERIA5String(issuerAltNames[i]));
					continue;
				}

				// DirNames
				if (issuerAltNames[i].contains(";") || issuerAltNames[i].contains("=")) {
					String x500Name = "";
					String[] parts = issuerAltNames[i].split(";");
					for (String name : parts) {
						x500Name += name + ",";
					}
					x500Name = x500Name.substring(0, x500Name.length() - 1);
					generalNames[i] = new GeneralName(GeneralName.directoryName, x500Name);
					continue;
				}

				// ediPartyName
				/*
				Pattern ediPattern = Pattern.compile("^edi:.+$");
				Matcher ediMatcher = ediPattern.matcher(issuerAltNames[i]);
				if (ediMatcher.matches()) {
					ASN1EncodableVector vec = new ASN1EncodableVector();
					vec.add(new DirectoryString(issuerAltNames[i].split("edi:")[1]));
					DERSequence seq = new DERSequence(vec);
					generalNames[i] = new GeneralName(GeneralName.ediPartyName, seq);
					continue;
				}
				*/
				// x400address
				//generalNames[i] = new GeneralName(GeneralName.x400Address, new X500Name("C=ETF,ST=SER"));

			}
			certificateBuilder.addExtension(Extension.issuerAlternativeName, criticalAltNames,
					new GeneralNames(generalNames));

		}

		// Certificate Policies
		if (anyPolicyChecked) {
			PolicyQualifierInfo policyQualifierInfo = new PolicyQualifierInfo(cpsUri);
			PolicyInformation policyInformation = new PolicyInformation(new ASN1ObjectIdentifier("2.5.29.32.0"),
					new DERSequence(policyQualifierInfo));
			certificateBuilder.addExtension(Extension.certificatePolicies, criticalCerPolicies,
					new CertificatePolicies(policyInformation));
		}

		return certificateBuilder.build(cs);
	}

	private PublicKey getPublicKeyFromCSR() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
		SubjectPublicKeyInfo puInfo = csr.getSubjectPublicKeyInfo();
		RSAKeyParameters rsaParam = (RSAKeyParameters) PublicKeyFactory.createKey(puInfo);
		RSAPublicKeySpec rsaSpec = new RSAPublicKeySpec(rsaParam.getModulus(), rsaParam.getExponent());
		KeyFactory kf = KeyFactory.getInstance("RSA");
		PublicKey pu = kf.generatePublic(rsaSpec);
		return pu;
	}

	public int getCaPathLength() {
		return caPathLength;
	}

	public void setCaPathLength(int caPathLength) {
		this.caPathLength = caPathLength;
	}

	public boolean isCriticalCerPolicies() {
		return criticalCerPolicies;
	}

	public void setCriticalCerPolicies(boolean criticalCerPolicies) {
		this.criticalCerPolicies = criticalCerPolicies;
	}

	public boolean isCriticalAltNames() {
		return criticalAltNames;
	}

	public void setCriticalAltNames(boolean criticalAltNames) {
		this.criticalAltNames = criticalAltNames;
	}

	public boolean isCriticalBasicConst() {
		return criticalBasicConst;
	}

	public void setCriticalBasicConst(boolean criticalBasicConst) {
		this.criticalBasicConst = criticalBasicConst;
	}

	public String getCpsUri() {
		return cpsUri;
	}

	public void setCpsUri(String cpsUri) {
		this.cpsUri = cpsUri;
	}

	public String[] getIssuerAltNames() {
		return issuerAltNames;
	}

	public void setIssuerAltNames(String[] issuerAltNames) {
		this.issuerAltNames = issuerAltNames;
	}

	public boolean isAnyPolicyChecked() {
		return anyPolicyChecked;
	}

	public void setAnyPolicyChecked(boolean anyPolicyChecked) {
		this.anyPolicyChecked = anyPolicyChecked;
	}

	public PKCS10CertificationRequest getCsr() {
		return csr;
	}

	public void setCsr(PKCS10CertificationRequest csr) {
		this.csr = csr;
	}
}
